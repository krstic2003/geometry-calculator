<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circle extends Model
{
    public static function circumference($radius = 0)
    {
        if ($radius != 0) {
        	return 2 * (float)$radius * pi();
        }else{
        	return 0;
        }
    }

    public static function surface($radius = 0)
    {
        if ($radius != 0) {
        	return pow($radius, 2) * pi();
        }else{
        	return 0;
        }
    }
}
