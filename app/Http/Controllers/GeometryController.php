<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Triangle;
use App\Circle;

use App\Library\Services\SumAll;

class GeometryController extends Controller
{
    public function triangle($a = 0, $b = 0, $c = 0)
    {
    	$circ = Triangle::circumference($a, $b, $c);
    	$surf = Triangle::surface($a, $b, $c);
    	$data = [
    		"type" => "triangle",
    		"a" => number_format((float)$a, 2, '.', ''),
			"b" => number_format((float)$b, 2, '.', ''),
			"c" => number_format((float)$c, 2, '.', ''),
			"surface" => number_format((float)$surf, 2, '.', ''),
			"circumference" => number_format((float)$circ, 2, '.', '')
    	];

    	if($circ == 0 || $surf == 0){
    		return "Error: Invalid Triangle Parameters!";
    	}else{
    		return response()->json($data);
    	}
    }

    public function circle($radius = 0)
    {
    	$circ = Circle::circumference($radius);
    	$surf = Circle::surface($radius);
    	$data = [
    		"type" => "circle",
    		"radius" => number_format((float)$radius, 2, '.', ''),
			"surface" => number_format((float)$surf, 2, '.', ''),
			"circumference" => number_format((float)$circ, 2, '.', '')
    	];

    	if($circ == 0 || $surf == 0){
    		return "Error: Invalid Circle Radius!";
    	}else{
    		return response()->json($data);
    	}
    	
    }

    public function sumSurfaces(SumAll $sumAll, $a = 0, $b = 0, $c = 0, $radius = 0)
    {
        return $sumAll->sumSurf($a, $b, $c, $radius);
    }

    public function sumCircumference(SumAll $sumAll, $a = 0, $b = 0, $c = 0, $radius = 0)
    {
        return $sumAll->sumCirc($a, $b, $c, $radius);
    }
}
