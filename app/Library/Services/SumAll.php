<?php
namespace App\Library\Services;
use App\Triangle;
use App\Circle;
  
class SumAll
{
    public function sumSurf($a = 0, $b = 0, $c = 0, $radius = 0)
    {
    	$surf_c = Circle::surface($radius);
    	$surf_t = Triangle::surface($a, $b, $c);
    	$sum = (float)$surf_c + (float)$surf_t;

    	return number_format((float)$sum, 2, '.', '');
    }

    public function sumCirc($a = 0, $b = 0, $c = 0, $radius = 0)
    {
    	$circ_c = Circle::circumference($radius);
    	$circ_t = Triangle::circumference($a, $b, $c);
    	$sum = (float)$circ_c + (float)$circ_t;

    	return number_format((float)$sum, 2, '.', '');
    }
}