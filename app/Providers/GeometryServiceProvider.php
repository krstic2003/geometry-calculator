<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Library\Services\SumAll;

class GeometryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\SumAll', function ($app) {
          return new SumAll();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
