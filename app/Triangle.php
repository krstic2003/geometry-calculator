<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Triangle extends Model
{
    public static function circumference($a = 0, $b = 0, $c = 0)
    {
        if ( $a != 0 && $b != 0 && $c != 0 ) {
        	return (float)$a + (float)$b + (float)$c;
        }else{
        	return 0;
        }
    }

    public static function surface($a = 0, $b = 0, $c = 0)
    {
        if ( $a != 0 && $b != 0 && $c != 0 ) {
        	//Heron's formula
        	$a = (float)$a;
        	$b = (float)$b;
        	$c = (float)$c;
        	$s = ( $a + $b + $c ) / 2;
        	$area = sqrt( $s*($s-$a)*($s-$b)*($s-$c) );
        	return $area;
        }else{
        	return 0;
        }
    }
}
