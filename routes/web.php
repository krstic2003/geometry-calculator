<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/triangle/{a}/{b}/{c}', 'GeometryController@triangle');
Route::get('/circle/{radius}', 'GeometryController@circle');

// Service routes - uncomment for testing
//Route::get('/sum-s/{a}/{b}/{c}/{radius}', 'GeometryController@sumSurfaces');
//Route::get('/sum-c/{a}/{b}/{c}/{radius}', 'GeometryController@sumCircumference');